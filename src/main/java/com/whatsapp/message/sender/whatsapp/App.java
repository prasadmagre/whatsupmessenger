package com.whatsapp.message.sender.whatsapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Hello world!
 *
 */
public class App {
	
	static WebDriver d=null;
	/*public static void main(String[] args) throws IOException {
		File file = new File("C:\\Users\\prmagre\\Desktop\\New.txt");
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		StringBuffer stringBuffer = new StringBuffer();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			// System.out.println(line);
			// stringBuffer.append(line);
			// stringBuffer.append("\n");
			// System.out.println(stringBuffer.toString());
			Pattern pattern = Pattern.compile("\\D*(\\d+)\\D*");
			Matcher matcher = pattern.matcher(line);

			try {
				if (matcher.find()) {
					String stringDigit = matcher.group(1);
					int number = Integer.parseInt(stringDigit);
					System.out.println(number);

				}
			} catch (Exception e) {

			}

		}
		fileReader.close();
		System.out.println("Contents of file:");
		// System.out.println(stringBuffer.toString());

		
		 * Pattern pattern = Pattern.compile("\\D*(\\d+)\\D*"); Matcher matcher =
		 * pattern.matcher("Abc defghijk lmn opqr st uvwxyza bc 19");
		 * 
		 * try { if (matcher.find()) { String stringDigit = matcher.group(1); int number
		 * = Integer.parseInt(stringDigit); System.out.println(number);
		 * 
		 * } } catch(Exception e) {
		 * 
		 * }
		 
	}*/
	
	

	@Test
	public  void test() throws InterruptedException, ParseException, InvalidFormatException, IOException {
		System.setProperty("webdriver.chrome.driver",
				this.getClass().getClassLoader().getResource("chromedriver.exe").getFile());
		d = new ChromeDriver();

		d.get("https://web.whatsapp.com/");
		Thread.sleep(10000);
		MessageReaderFromExcel msgReader= new MessageReaderFromExcel();
		Map<Integer, Map<String, String>> allRowsData = msgReader.getAllMessage();

		for (Integer name : allRowsData.keySet()) {
			if (allRowsData.get(name).get("IsActive").equalsIgnoreCase("Yes")) {

				d.findElement(By.xpath("//span[text()='" + allRowsData.get(name).get("Contact Name") + "']")).click();
				String message = "";
				if (allRowsData.get(name).get("RandomMessage").equalsIgnoreCase("Yes")) {
					final String[] allMessage = allRowsData.get(name).get("Message").split("\\|");
					Random random = new Random();
					int index = random.nextInt(allMessage.length);
					//System.out.println(allMessage[index]);
					message=allMessage[index];					

				}else {
					message=allRowsData.get(name).get("Message");
				}
				d.findElement(By.xpath("//div[@class='pluggable-input-body copyable-text selectable-text']"))
						.sendKeys(message);
				Thread.sleep(700);
				d.findElement(By.xpath("//span[@data-icon='send']")).click();
			}
		}
		d.close();
		d.quit();

	}
	
	@AfterMethod
	public static void closeBrowser() {
		d.close();
		d.quit();
	}
}
