package com.whatsapp.message.sender.whatsapp;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MessageReaderFromExcel {

	public  Map<Integer, Map<String, String>> getAllMessage() throws InvalidFormatException, IOException{
		
		Map<Integer, Map<String, String>> allRowsData = new HashMap<Integer, Map<String, String>>();
		
		File file = new File(this.getClass().getClassLoader().getResource("MessageToSend.xlsx").getFile());
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		
		XSSFSheet firstSheet = workbook.getSheetAt(0);
		
		int totalNoRow= firstSheet.getLastRowNum();
		
		for(int i=1; i<=totalNoRow;i++) {
			//String contactName=getcellValue(firstSheet.getRow(i).getCell(0));
			Map<String, String> innerMap= new HashMap<String, String>();
			innerMap.put(getcellValue(firstSheet.getRow(0).getCell(0)), getcellValue(firstSheet.getRow(i).getCell(0)));
			innerMap.put(getcellValue(firstSheet.getRow(0).getCell(1)), getcellValue(firstSheet.getRow(i).getCell(1)));
			innerMap.put(getcellValue(firstSheet.getRow(0).getCell(2)), getcellValue(firstSheet.getRow(i).getCell(2)));
			innerMap.put(getcellValue(firstSheet.getRow(0).getCell(3)), getcellValue(firstSheet.getRow(i).getCell(3)));
			allRowsData.put(i, innerMap);
			
		}
		System.out.println("data -> " + allRowsData);
		
		
		
		return allRowsData;

	}
	
	public static void main(String[] args) throws InvalidFormatException, IOException {
		//getAllMessage();
	}
	
	public static String getcellValue(Cell cell) {
		String cellValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			// System.out.print(cell.getStringCellValue());
			cellValue = cell.getStringCellValue().trim();
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			// System.out.print(cell.getBooleanCellValue());
			cellValue = String.valueOf(cell.getBooleanCellValue()).trim();

			break;
		case Cell.CELL_TYPE_NUMERIC:
			// System.out.print(cell.getNumericCellValue());
			Double value = cell.getNumericCellValue();
			Long longValue = value.longValue();
			cellValue = new String(longValue.toString()).trim();
			break;
		case Cell.CELL_TYPE_FORMULA:
			// System.out.print(cell.getNumericCellValue());
			cellValue = cell.getStringCellValue();

			break;
		}
		//System.out.println(cellValue);
		return cellValue;

	}
}
